<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_table.proto

namespace Ydb\Table;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>Ydb.Table.DescribeTableResult</code>
 */
class DescribeTableResult extends \Google\Protobuf\Internal\Message
{
    /**
     * Description of scheme object
     *
     * Generated from protobuf field <code>.Ydb.Scheme.Entry self = 1;</code>
     */
    protected $self = null;
    /**
     * List of columns
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.ColumnMeta columns = 2;</code>
     */
    private $columns;
    /**
     * List of primary key columns
     *
     * Generated from protobuf field <code>repeated string primary_key = 3;</code>
     */
    private $primary_key;
    /**
     * List of key ranges for shard
     *
     * Generated from protobuf field <code>repeated .Ydb.TypedValue shard_key_bounds = 4;</code>
     */
    private $shard_key_bounds;
    /**
     * List of indexes
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.TableIndex indexes = 5;</code>
     */
    private $indexes;
    /**
     * Statistics of table
     *
     * Generated from protobuf field <code>.Ydb.Table.TableStats table_stats = 6;</code>
     */
    protected $table_stats = null;
    /**
     * TTL params
     *
     * Generated from protobuf field <code>.Ydb.Table.TtlSettings ttl_settings = 7;</code>
     */
    protected $ttl_settings = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Ydb\Scheme\Entry $self
     *           Description of scheme object
     *     @type \Ydb\Table\ColumnMeta[]|\Google\Protobuf\Internal\RepeatedField $columns
     *           List of columns
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $primary_key
     *           List of primary key columns
     *     @type \Ydb\TypedValue[]|\Google\Protobuf\Internal\RepeatedField $shard_key_bounds
     *           List of key ranges for shard
     *     @type \Ydb\Table\TableIndex[]|\Google\Protobuf\Internal\RepeatedField $indexes
     *           List of indexes
     *     @type \Ydb\Table\TableStats $table_stats
     *           Statistics of table
     *     @type \Ydb\Table\TtlSettings $ttl_settings
     *           TTL params
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api\Protos\YdbTable::initOnce();
        parent::__construct($data);
    }

    /**
     * Description of scheme object
     *
     * Generated from protobuf field <code>.Ydb.Scheme.Entry self = 1;</code>
     * @return \Ydb\Scheme\Entry
     */
    public function getSelf()
    {
        return isset($this->self) ? $this->self : null;
    }

    public function hasSelf()
    {
        return isset($this->self);
    }

    public function clearSelf()
    {
        unset($this->self);
    }

    /**
     * Description of scheme object
     *
     * Generated from protobuf field <code>.Ydb.Scheme.Entry self = 1;</code>
     * @param \Ydb\Scheme\Entry $var
     * @return $this
     */
    public function setSelf($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Scheme\Entry::class);
        $this->self = $var;

        return $this;
    }

    /**
     * List of columns
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.ColumnMeta columns = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * List of columns
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.ColumnMeta columns = 2;</code>
     * @param \Ydb\Table\ColumnMeta[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setColumns($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ydb\Table\ColumnMeta::class);
        $this->columns = $arr;

        return $this;
    }

    /**
     * List of primary key columns
     *
     * Generated from protobuf field <code>repeated string primary_key = 3;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getPrimaryKey()
    {
        return $this->primary_key;
    }

    /**
     * List of primary key columns
     *
     * Generated from protobuf field <code>repeated string primary_key = 3;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setPrimaryKey($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->primary_key = $arr;

        return $this;
    }

    /**
     * List of key ranges for shard
     *
     * Generated from protobuf field <code>repeated .Ydb.TypedValue shard_key_bounds = 4;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getShardKeyBounds()
    {
        return $this->shard_key_bounds;
    }

    /**
     * List of key ranges for shard
     *
     * Generated from protobuf field <code>repeated .Ydb.TypedValue shard_key_bounds = 4;</code>
     * @param \Ydb\TypedValue[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setShardKeyBounds($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ydb\TypedValue::class);
        $this->shard_key_bounds = $arr;

        return $this;
    }

    /**
     * List of indexes
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.TableIndex indexes = 5;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getIndexes()
    {
        return $this->indexes;
    }

    /**
     * List of indexes
     *
     * Generated from protobuf field <code>repeated .Ydb.Table.TableIndex indexes = 5;</code>
     * @param \Ydb\Table\TableIndex[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setIndexes($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ydb\Table\TableIndex::class);
        $this->indexes = $arr;

        return $this;
    }

    /**
     * Statistics of table
     *
     * Generated from protobuf field <code>.Ydb.Table.TableStats table_stats = 6;</code>
     * @return \Ydb\Table\TableStats
     */
    public function getTableStats()
    {
        return isset($this->table_stats) ? $this->table_stats : null;
    }

    public function hasTableStats()
    {
        return isset($this->table_stats);
    }

    public function clearTableStats()
    {
        unset($this->table_stats);
    }

    /**
     * Statistics of table
     *
     * Generated from protobuf field <code>.Ydb.Table.TableStats table_stats = 6;</code>
     * @param \Ydb\Table\TableStats $var
     * @return $this
     */
    public function setTableStats($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Table\TableStats::class);
        $this->table_stats = $var;

        return $this;
    }

    /**
     * TTL params
     *
     * Generated from protobuf field <code>.Ydb.Table.TtlSettings ttl_settings = 7;</code>
     * @return \Ydb\Table\TtlSettings
     */
    public function getTtlSettings()
    {
        return isset($this->ttl_settings) ? $this->ttl_settings : null;
    }

    public function hasTtlSettings()
    {
        return isset($this->ttl_settings);
    }

    public function clearTtlSettings()
    {
        unset($this->ttl_settings);
    }

    /**
     * TTL params
     *
     * Generated from protobuf field <code>.Ydb.Table.TtlSettings ttl_settings = 7;</code>
     * @param \Ydb\Table\TtlSettings $var
     * @return $this
     */
    public function setTtlSettings($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Table\TtlSettings::class);
        $this->ttl_settings = $var;

        return $this;
    }

}

