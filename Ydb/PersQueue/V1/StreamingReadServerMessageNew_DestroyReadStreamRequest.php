<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_persqueue_v1.proto

namespace Ydb\PersQueue\V1;

if (false) {
    /**
     * This class is deprecated. Use Ydb\PersQueue\V1\StreamingReadServerMessageNew\DestroyReadStreamRequest instead.
     * @deprecated
     */
    class StreamingReadServerMessageNew_DestroyReadStreamRequest {}
}
class_exists(StreamingReadServerMessageNew\DestroyReadStreamRequest::class);
@trigger_error('Ydb\PersQueue\V1\StreamingReadServerMessageNew_DestroyReadStreamRequest is deprecated and will be removed in the next major release. Use Ydb\PersQueue\V1\StreamingReadServerMessageNew\DestroyReadStreamRequest instead', E_USER_DEPRECATED);

