<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_persqueue_v1.proto

namespace Ydb\PersQueue\V1\StreamingReadClientMessage;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Signal for server that client processed some read data.
 *
 * Generated from protobuf message <code>Ydb.PersQueue.V1.StreamingReadClientMessage.Commit</code>
 */
class Commit extends \Google\Protobuf\Internal\Message
{
    /**
     * Partition read cookies that indicates processed data.
     *
     * Generated from protobuf field <code>repeated .Ydb.PersQueue.V1.CommitCookie cookies = 1;</code>
     */
    private $cookies;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Ydb\PersQueue\V1\CommitCookie[]|\Google\Protobuf\Internal\RepeatedField $cookies
     *           Partition read cookies that indicates processed data.
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api\Protos\YdbPersqueueV1::initOnce();
        parent::__construct($data);
    }

    /**
     * Partition read cookies that indicates processed data.
     *
     * Generated from protobuf field <code>repeated .Ydb.PersQueue.V1.CommitCookie cookies = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * Partition read cookies that indicates processed data.
     *
     * Generated from protobuf field <code>repeated .Ydb.PersQueue.V1.CommitCookie cookies = 1;</code>
     * @param \Ydb\PersQueue\V1\CommitCookie[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setCookies($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ydb\PersQueue\V1\CommitCookie::class);
        $this->cookies = $arr;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Commit::class, \Ydb\PersQueue\V1\StreamingReadClientMessage_Commit::class);

