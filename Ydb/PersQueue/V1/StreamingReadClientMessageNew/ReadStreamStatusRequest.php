<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_persqueue_v1.proto

namespace Ydb\PersQueue\V1\StreamingReadClientMessageNew;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>Ydb.PersQueue.V1.StreamingReadClientMessageNew.ReadStreamStatusRequest</code>
 */
class ReadStreamStatusRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 read_stream_id = 1;</code>
     */
    protected $read_stream_id = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $read_stream_id
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api\Protos\YdbPersqueueV1::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 read_stream_id = 1;</code>
     * @return int|string
     */
    public function getReadStreamId()
    {
        return $this->read_stream_id;
    }

    /**
     * Generated from protobuf field <code>int64 read_stream_id = 1;</code>
     * @param int|string $var
     * @return $this
     */
    public function setReadStreamId($var)
    {
        GPBUtil::checkInt64($var);
        $this->read_stream_id = $var;

        return $this;
    }

}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(ReadStreamStatusRequest::class, \Ydb\PersQueue\V1\StreamingReadClientMessageNew_ReadStreamStatusRequest::class);

