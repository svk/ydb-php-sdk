<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_cms.proto

namespace Ydb\Cms;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>Ydb.Cms.GetDatabaseStatusResult</code>
 */
class GetDatabaseStatusResult extends \Google\Protobuf\Internal\Message
{
    /**
     * Full path to database's home dir.
     *
     * Generated from protobuf field <code>string path = 1;</code>
     */
    protected $path = '';
    /**
     * Current database state.
     *
     * Generated from protobuf field <code>.Ydb.Cms.GetDatabaseStatusResult.State state = 2;</code>
     */
    protected $state = 0;
    /**
     * Database resources requested for allocation.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources required_resources = 3;</code>
     */
    protected $required_resources = null;
    /**
     * Database resources allocated by CMS.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources allocated_resources = 4;</code>
     */
    protected $allocated_resources = null;
    /**
     * Externally allocated database resources registered in CMS.
     *
     * Generated from protobuf field <code>repeated .Ydb.Cms.AllocatedComputationalUnit registered_resources = 5;</code>
     */
    private $registered_resources;
    /**
     * Current database generation. Incremented at each successful
     * alter request.
     *
     * Generated from protobuf field <code>uint64 generation = 6;</code>
     */
    protected $generation = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $path
     *           Full path to database's home dir.
     *     @type int $state
     *           Current database state.
     *     @type \Ydb\Cms\Resources $required_resources
     *           Database resources requested for allocation.
     *     @type \Ydb\Cms\Resources $allocated_resources
     *           Database resources allocated by CMS.
     *     @type \Ydb\Cms\AllocatedComputationalUnit[]|\Google\Protobuf\Internal\RepeatedField $registered_resources
     *           Externally allocated database resources registered in CMS.
     *     @type int|string $generation
     *           Current database generation. Incremented at each successful
     *           alter request.
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api\Protos\YdbCms::initOnce();
        parent::__construct($data);
    }

    /**
     * Full path to database's home dir.
     *
     * Generated from protobuf field <code>string path = 1;</code>
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Full path to database's home dir.
     *
     * Generated from protobuf field <code>string path = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setPath($var)
    {
        GPBUtil::checkString($var, True);
        $this->path = $var;

        return $this;
    }

    /**
     * Current database state.
     *
     * Generated from protobuf field <code>.Ydb.Cms.GetDatabaseStatusResult.State state = 2;</code>
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Current database state.
     *
     * Generated from protobuf field <code>.Ydb.Cms.GetDatabaseStatusResult.State state = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setState($var)
    {
        GPBUtil::checkEnum($var, \Ydb\Cms\GetDatabaseStatusResult\State::class);
        $this->state = $var;

        return $this;
    }

    /**
     * Database resources requested for allocation.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources required_resources = 3;</code>
     * @return \Ydb\Cms\Resources
     */
    public function getRequiredResources()
    {
        return isset($this->required_resources) ? $this->required_resources : null;
    }

    public function hasRequiredResources()
    {
        return isset($this->required_resources);
    }

    public function clearRequiredResources()
    {
        unset($this->required_resources);
    }

    /**
     * Database resources requested for allocation.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources required_resources = 3;</code>
     * @param \Ydb\Cms\Resources $var
     * @return $this
     */
    public function setRequiredResources($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Cms\Resources::class);
        $this->required_resources = $var;

        return $this;
    }

    /**
     * Database resources allocated by CMS.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources allocated_resources = 4;</code>
     * @return \Ydb\Cms\Resources
     */
    public function getAllocatedResources()
    {
        return isset($this->allocated_resources) ? $this->allocated_resources : null;
    }

    public function hasAllocatedResources()
    {
        return isset($this->allocated_resources);
    }

    public function clearAllocatedResources()
    {
        unset($this->allocated_resources);
    }

    /**
     * Database resources allocated by CMS.
     *
     * Generated from protobuf field <code>.Ydb.Cms.Resources allocated_resources = 4;</code>
     * @param \Ydb\Cms\Resources $var
     * @return $this
     */
    public function setAllocatedResources($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Cms\Resources::class);
        $this->allocated_resources = $var;

        return $this;
    }

    /**
     * Externally allocated database resources registered in CMS.
     *
     * Generated from protobuf field <code>repeated .Ydb.Cms.AllocatedComputationalUnit registered_resources = 5;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getRegisteredResources()
    {
        return $this->registered_resources;
    }

    /**
     * Externally allocated database resources registered in CMS.
     *
     * Generated from protobuf field <code>repeated .Ydb.Cms.AllocatedComputationalUnit registered_resources = 5;</code>
     * @param \Ydb\Cms\AllocatedComputationalUnit[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setRegisteredResources($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Ydb\Cms\AllocatedComputationalUnit::class);
        $this->registered_resources = $arr;

        return $this;
    }

    /**
     * Current database generation. Incremented at each successful
     * alter request.
     *
     * Generated from protobuf field <code>uint64 generation = 6;</code>
     * @return int|string
     */
    public function getGeneration()
    {
        return $this->generation;
    }

    /**
     * Current database generation. Incremented at each successful
     * alter request.
     *
     * Generated from protobuf field <code>uint64 generation = 6;</code>
     * @param int|string $var
     * @return $this
     */
    public function setGeneration($var)
    {
        GPBUtil::checkUint64($var);
        $this->generation = $var;

        return $this;
    }

}

