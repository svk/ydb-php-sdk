<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: api/protos/ydb_export.proto

namespace Ydb\Export;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>Ydb.Export.ExportToYtMetadata</code>
 */
class ExportToYtMetadata extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportToYtSettings settings = 1;</code>
     */
    protected $settings = null;
    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportProgress.Progress progress = 2;</code>
     */
    protected $progress = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Ydb\Export\ExportToYtSettings $settings
     *     @type int $progress
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Api\Protos\YdbExport::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportToYtSettings settings = 1;</code>
     * @return \Ydb\Export\ExportToYtSettings
     */
    public function getSettings()
    {
        return isset($this->settings) ? $this->settings : null;
    }

    public function hasSettings()
    {
        return isset($this->settings);
    }

    public function clearSettings()
    {
        unset($this->settings);
    }

    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportToYtSettings settings = 1;</code>
     * @param \Ydb\Export\ExportToYtSettings $var
     * @return $this
     */
    public function setSettings($var)
    {
        GPBUtil::checkMessage($var, \Ydb\Export\ExportToYtSettings::class);
        $this->settings = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportProgress.Progress progress = 2;</code>
     * @return int
     */
    public function getProgress()
    {
        return $this->progress;
    }

    /**
     * Generated from protobuf field <code>.Ydb.Export.ExportProgress.Progress progress = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setProgress($var)
    {
        GPBUtil::checkEnum($var, \Ydb\Export\ExportProgress\Progress::class);
        $this->progress = $var;

        return $this;
    }

}

