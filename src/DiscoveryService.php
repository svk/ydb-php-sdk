<?php
namespace svk\YdbSdk;

class DiscoveryService
{
    protected $client;
    private $auth_metadata;

    public function __construct($endpoint, $token, $database)
    {
        $this->client = new \Ydb\Discovery\V1\DiscoveryServiceClient($endpoint, [
            'credentials' => \Grpc\ChannelCredentials::createSsl(),
        ]);

        $this->auth_metadata = [
            'x-ydb-auth-ticket' => [$token],
            'x-ydb-database' => [$database],
        ];

        return true;
    }

    public function WhoAmI()
    {
        return $this->req('WhoAmI')->getUser();
    }

    private function req($reqName, $data=[])
    {
        $requestClassName = '\\Ydb\\Discovery\\'.$reqName.'Request';
        $resultClassName = '\\Ydb\\Discovery\\'.$reqName.'Result';
        $req = new $requestClassName();
        
        list($t, $result) = $this->client->$reqName($req, $this->auth_metadata)->wait();

        $result =  $t->getOperation()->getResult()->serializeToJsonString();

        $r = new $resultClassName();
        $r->mergeFromJsonString($result);

        $this->result = $r;

        return $this->result;
    }
}