<?php
namespace svk\YdbSdk;

class TableService
{
    protected $client;
    private $session_id;
    private $auth_metadata;
    private $path;
    private $lasterror;
    private $lasterrorcode;

    public function __construct($endpoint, $token, $database)
    {
        $this->client = new \Ydb\Table\V1\TableServiceClient($endpoint, [
            'credentials' => \Grpc\ChannelCredentials::createSsl(),
        ]);


        $this->auth_metadata = [
            'x-ydb-auth-ticket' => [$token],
            'x-ydb-database' => [$database],
        ];
        $this->path = $database;

        return true;
    }

    public function createSession()
    {
        $this->session_id =  $this->req('CreateSession')->getSessionId();
    }

    public function describeTable($table)
    {
        if(substr($table,0,1)!='/')
        {
            $table = $this->path.'/'.$table;
        }
        $result = $this->req('DescribeTable', [
            'session_id' => $this->session_id,
            'path' => $table,
            'include_table_stats' => true,
        ]);

        return json_decode($result->serializeToJsonString(), true);
    }

    public function query($yql, $tx_id)
    {
        $q = new \Ydb\Table\Query(['yql_text' => $yql]);

        $transaction_settings = new \Ydb\Table\TransactionSettings();
        $transaction_control = new \Ydb\Table\TransactionControl([
            'tx_id' => $tx_id,
            'commit_tx' => true,
        ]);
        $query_cache_policy = new \Ydb\Table\QueryCachePolicy();
        $operation_params = new \Ydb\Operations\OperationParams();

        $result = $this->req('ExecuteDataQuery',
            [
            'session_id' => $this->session_id,
            'query' => $q,
            'tx_control' => $transaction_control,
            'query_cache_policy' => $query_cache_policy,
            'operation_params' => $operation_params,
            'collect_stats' => 1,
        ]);

        return $result;
    }

    public function beginTransaction()
    {
        $serializable_read_write = new \Ydb\Table\SerializableModeSettings();
        $online_read_only = new \Ydb\Table\OnlineModeSettings();
        $stale_read_only = new \Ydb\Table\StaleModeSettings();

        $transaction_settings = new \Ydb\Table\TransactionSettings([
            'serializable_read_write' => $serializable_read_write,
        ]);
        $operation_params = new \Ydb\Operations\OperationParams();

        $result = $this->req('BeginTransaction',
            [
            'session_id' => $this->session_id,
            'tx_settings' => $transaction_settings,
            'operation_params' => $operation_params,
        ]);

        return $result->getTxMeta()->getId();
    }

    public function commitTransaction($tx_id)
    {
        $operation_params = new \Ydb\Operations\OperationParams();

        $result = $this->req('CommitTransaction', [
            'session_id' => $this->session_id,
            'tx_id' => $tx_id,
            'operation_params' => $operation_params,
        ]);

        return true;
    }

    public function dropTable($table)
    {
        $operation_params = new \Ydb\Operations\OperationParams();
        return $this->req('DropTable', [
            'path' => $this->path.'/'.$path,
            'session_id' => $this->session_id,
            'operation_params' => $operation_params,
        ]);
    }

    private function req($reqName, $data=[])
    {
        $requestClassName = '\\Ydb\\Table\\'.$reqName.'Request';
        $resultClassName = '\\Ydb\\Table\\'.$reqName.'Result';
        $req = new $requestClassName($data);
        
        list($t, $result) = $this->client->$reqName($req, $this->auth_metadata)->wait();

        $status = $t->getOperation()->getStatus();

        if($status == \Ydb\StatusIds\StatusCode::SUCCESS)
        {
            $result =  $t->getOperation()->getResult()->serializeToJsonString();

            if($reqName == 'ExecuteDataQuery')
            {
                $resultClassName =  '\\Ydb\\Table\\ExecuteQueryResult';
            }

            $r = new $resultClassName();
            $r->mergeFromJsonString($result);

            $this->result = $r;

            return $this->result;
        } else
        {
            $statusName = \Ydb\StatusIds\StatusCode::name($status);
            //print "Error during $reqName invocation: $statusName\n";
            
            $message = $t->getOperation()->getIssues()->getIterator()->current()->getMessage();
            $this->lasterror = $message;
            $this->lasterrorcode = $statusName;

            return false;
        }
    }

    public function getLastError()
    {
        return $this->lasterror;
    }
    public function getLastErrorCode()
    {
        return $this->lasterrorcode;
    }
}