<?php
namespace svk\YdbSdk;

class SchemeService
{
    protected $client;
    private $path;
    private $auth_metadata;
    private $lasterror;
    private $lasterrorcode;

    public function __construct($endpoint, $token, $database)
    {
        $this->client = new \Ydb\Scheme\V1\SchemeServiceClient($endpoint, [
            'credentials' => \Grpc\ChannelCredentials::createSsl(),
        ]);


        $this->auth_metadata = [
            'x-ydb-auth-ticket' => [$token],
            'x-ydb-database' => [$database],
        ];
        $this->path = $database;

        return true;
    }

    public function listDirectory($path)
    {
        return $this->req('ListDirectory', [
            'path' => $this->path.'/'.$path,
        ]);
    }

    private function req($reqName, $data=[])
    {
        $requestClassName = '\\Ydb\\Scheme\\'.$reqName.'Request';
        $resultClassName = '\\Ydb\\Scheme\\'.$reqName.'Result';
        $req = new $requestClassName($data);
        
        list($t, $result) = $this->client->$reqName($req, $this->auth_metadata)->wait();

        $status = $t->getOperation()->getStatus();

        if($status == \Ydb\StatusIds\StatusCode::SUCCESS)
        {
            $result =  $t->getOperation()->getResult()->serializeToJsonString();

            $r = new $resultClassName();
            $r->mergeFromJsonString($result);

            $this->result = $r;

            return $this->result;
        } else
        {
            $statusName = \Ydb\StatusIds\StatusCode::name($status);
            //print "Error during $reqName invocation: $statusName\n";
            
            $message = $t->getOperation()->getIssues()->getIterator()->current()->getMessage();
            $this->lasterror = $message;
            $this->lasterrorcode = $statusName;

            return false;
        }
    }

    public function getLastError()
    {
        return $this->lasterror;
    }
    public function getLastErrorCode()
    {
        return $this->lasterrorcode;
    }
}
